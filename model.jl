function prob(T, ρ, n, p, γ) #add the fact that there could be reward!!!
    phnrw = zeros(Float64, n+1)
    plnrw = zeros(Float64, n+1)
    stay_cdf = zeros(Float64, n+1)
    stay_cdf[1] = 1.
    phnrw[1] = 1.
    plnrw[1] = 0.
    for i = 1:n
        phnrw[i+1] = phnrw[i]*(1-γ)*(1-p)
        plnrw[i+1] = phnrw[i]*γ+plnrw[i]
        stay_cdf[i+1] = cdf(Poisson(i*ρ),T)
    end
    leave_pdf = -diff(stay_cdf).*(phnrw+plnrw)[1:n]
    leave_pdf = leave_pdf/sum(leave_pdf)
    axis = collect(0:(n-1))
    v_mean = mean(axis,WeightVec(leave_pdf))
    v_std = std(axis,WeightVec(leave_pdf))
    return leave_pdf, v_mean, v_std
end











function computeLikelihood(param, staying, leaving, MAX)

    if any(param .<= 0.001)
        return 1e15
    end
    if param[3] .>= 0.999
        return 1e15
    end

    thr, drift, lapse, var = param

    # computing effective threshold:
    eff_thr =  thr + var*drift

    # leavingdistr
    leaveDistr = InverseGaussian(eff_thr/drift,  (eff_thr)^2)
    # get cumulative leaving prob. and binned pdf
    #println(log(staying))

    #neg_log_lkl_stay = -log(lapse*(1-exp(-decay*staying))+
    #(1-lapse).*max(ones(size(staying))-cdf(leaveDistr, staying), 1e-10))
    #lkl_stay =
    #neg_log_lkl_leave = -log(lapse*exp(-decay*leaving).*decay +
    #(1-lapse).*max(pdf(leaveDistr, leaving),1e-10))

    neg_log_lkl_stay = -log(lapse*(MAX -(staying+var))./MAX+
    (1-lapse).*max(ones(size(staying))-cdf(leaveDistr, staying+var), 1e-10))
    #lkl_stay =
    neg_log_lkl_leave = -log(lapse./MAX +
    (1-lapse).*max(pdf(leaveDistr, leaving+var),1e-10))


    neg_log_lkl = sum(neg_log_lkl_leave) + sum(neg_log_lkl_stay)

    return neg_log_lkl

end

function minimizeLikelihood(staying, leaving, MAX, rt)
    parametri = [6., 2., 0.1, 1. ]

    res = Optim.optimize(t -> computeLikelihood(t, staying, leaving, MAX), parametri)

    return res.minimum
end

function getParam(df, cutoff; rt = true)
    staying, leaving = preprocessing(df, cutoff, rt)
    MAX = max(maximum(leaving),maximum(staying))
    param = minimizeLikelihood(staying, leaving, MAX, rt)
    return param
end
