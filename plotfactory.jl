using DataFrames
using PlotlyJS
using Colors
using Optim
using Rsvg
import AT
import PD
reload("PD")
reload("AT")

function kb_input(s,v)
  println(s)
  for (ind,val) in enumerate(v)
    println("$ind) $val")
  end
  return v[parse(chomp(readline()))]
end

#data = Dict()
load_data = kb_input("load_data", [true, false])
if load_data
  data = Dict();
  data[:streaks] = readtable("data/streakswtserthwtc.csv");
  data[:pokes] = readtable("data/pokeswtserthwtc.csv");
end
#data = PD.getall([PD.wt])
cs = Dict()
cs[:StreakNumber] = t -> (10 <= t <= 60)
cs[:StartHigh] = t -> t
cs[:LastReward] = t -> (t>=1)
cs[:Gen] = ["wt"]

colori = [colorant"#E24A33", colorant"#348ABD", colorant"#988ED5", colorant"#777777",
    colorant"#FBC15E",colorant"#8EBA42", colorant"#FFB5B8"]
selected_data = AT.choose_data(data[:streaks],cs)

x = kb_input("x = ?", [:LastReward,:AfterLast, :NumPokes])
y = kb_input("y = ?", [:LastReward,:AfterLast, :NumPokes, AT.gethazard, AT.gethist,AT.getcum])
z1 = kb_input("z1  = ?", [:Barrier,:Stim,:fake])
z2 = kb_input("z2 = ?", [:Protocol,:fake])
plot_func = kb_input("plot style?", [AT.myplot,AT.myscatter,AT.mybar])
traces = AT.getxy_plot(colori, selected_data,[z1,z2],x,y,
plot_func)
#legend=attr(x=0.65, y=.1))

s = plot(traces)
#display(s)
#savefig(s, "pdfs/$x$y$z1$z2.pdf")
